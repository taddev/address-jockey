import React, { useState, useEffect } from 'react';
import './App.scss';
import axios from 'axios';


function App() {
  const [users, setUsers] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (!loaded){
      axios.get('/api/users')
        .then((response) => {
          setUsers(response.data.data);
          setLoaded(true);
        });
    }
  });

  if (loaded) {
    return (
      <section className="section">
        <div className="container">
          <ul>
            {users.map(user => (
              <li key={user.id}>
                {user.id} - {user.firstName}
              </li>
            ))}
          </ul>
        </div>
      </section>
    );
  } else {
    return <div>Loading...</div>;
  }
}

export default App;
